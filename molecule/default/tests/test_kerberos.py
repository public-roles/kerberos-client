import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('kerberos')


def test_packages_installed(host):
    packages = [
        'krb5-user'
    ]
    for package in packages:
        assert host.package(package).is_installed


def test_kinit(host):
    host.check_output('echo vagrant | kinit vagrant@ANSIBLE.VAGRANT')
    klist = host.check_output('klist')
    assert 'Default principal: vagrant@ANSIBLE.VAGRANT' in klist
    assert 'krbtgt/ANSIBLE.VAGRANT@ANSIBLE.VAGRANT' in klist
