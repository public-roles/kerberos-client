Kerberos 
=========

Role to configure a kerberos client

Requirements
------------

None

Role Variables
--------------

* `kerberos_realm`: kerberos realm in uppercase
* `adds_domain`: kerberos domain
* `kdc_server_name`: domain controller server name

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      roles:
         - role: kerberos
           kerberos_realm: ANSIBLE.VAGRANT
           domain: ansible.vagrant
           kdc_server_name: dc1.ansible.vagrant

License
-------

BSD
